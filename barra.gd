extends ProgressBar

var audio_up
var audio_dano
const gameOver = preload("res://game_over.tscn")

func muda_valor():
	value -= 10
	audio_dano.play()
	if value <= 0:
		get_tree().root.add_child(gameOver.instantiate())
		get_node("/root/principal").queue_free()
		

func aumenta_valor():
	value += 5	
	audio_up.play()

func _ready():
	var jogador = get_node("/root/principal/Jogador/jogador")
	audio_up = get_node("/root/principal/audio_up")
	audio_dano = get_node("/root/principal/audio_dano")
	jogador.connect("tomou_dano", self.muda_valor)
	jogador.connect("power_up", self.aumenta_valor)
	
	
