extends CharacterBody2D

@export var speed = 600
@export var gravidade = 800
@export var pulo = -600

signal tomou_dano
signal power_up


func _physics_process(delta):
	velocity.y += delta * gravidade
	
	if Input.is_action_pressed("ui_left"):
		velocity.x = -speed
	elif Input.is_action_pressed("ui_right"):
		velocity.x = speed
	else:
		velocity.x = 0
		
	if Input.is_action_pressed("ui_up") and is_on_floor():
		velocity.y = pulo
		
	move_and_slide()
	
	for i in get_slide_collision_count():
		var collision = get_slide_collision(i)
		if collision.get_collider().name == 'inimigo':
			emit_signal("tomou_dano")
			$Sprite2D/animacao.play("dano")
			collision.get_collider().queue_free()
		elif collision.get_collider().name == 'powerUP':
			emit_signal("power_up")
			$Sprite2D/animacao.play("up")
			collision.get_collider().queue_free()
		
